package helb.mvanpee.controller;

import helb.mvanpee.model.Post;
import helb.mvanpee.model.Comment;
import helb.mvanpee.repository.PostRepository;
import helb.mvanpee.repository.CommentRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("comments")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class CommentController {

    @Inject
    CommentRepository commentRepository;
    @Inject
    PostRepository postRepository;

    @GET
    public List<Comment> getAll(@QueryParam("postId") Long postId) {
        return commentRepository.findAll(postId);
    }

    @POST
    @Path("/{post}")
    public Response create(Comment comment, @PathParam("post") Long postId) {
        Post p = postRepository.findPostById(postId);
        commentRepository.createComment(comment,p);
        return Response.status(201).build();

    }

    @PUT
    public Response update(Comment comment) {
        commentRepository.updateComment(comment);
        return Response.status(204).build();
    }

    @DELETE
    @Path("/{comment}")
    public Response delete(@PathParam("comment") Long commentId) {
        commentRepository.deleteComment(commentId);
        return Response.status(204).build();
    }

}
