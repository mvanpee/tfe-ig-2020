package helb.mvanpee.controller;

import helb.mvanpee.model.Post;
import helb.mvanpee.repository.PostRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("posts")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class PostController {

    @Inject
    PostRepository postRepository;

    @GET
    public List<Post> getAll(){
        return postRepository.findAll();
    }

    @POST
    public Response create(Post post){
        postRepository.createPost(post);
        return Response.status(201).build();
    }

    @PUT
    public Response update(Post post){
        postRepository.updatePost(post);
        return Response.status(204).build();
    }

    @DELETE
    public Response delete(@QueryParam("id") Long postId){
        postRepository.deletePost(postId);
        return Response.status(204).build();
    }
}
