package helb.mvanpee.repository;

import helb.mvanpee.model.Post;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.util.List;

@ApplicationScoped
public class PostRepository {

    @Inject
    EntityManager entityManager;

    public List<Post> findAll() {
        return entityManager.createNamedQuery("Posts.findAll", Post.class)
                .getResultList();
    }

    public Post findPostById(Long id){
        Post post = entityManager.find(Post.class, id);
        if(post == null){
            throw new WebApplicationException("Post with id of " + id + " does not exist.", 404);
        }
        return post;
    }

    @Transactional
    public void updatePost(Post post){
        Post postToUpdate = findPostById(post.getId());
        postToUpdate.setTitle(post.getTitle());
        postToUpdate.setDescription(post.getDescription());
        postToUpdate.setContent(post.getContent());
    }

    @Transactional
    public void createPost(Post post){
        entityManager.persist(post);
    }

    @Transactional
    public void deletePost(Long postId){
        Post p = findPostById(postId);
        entityManager.remove(p);
    }
}
