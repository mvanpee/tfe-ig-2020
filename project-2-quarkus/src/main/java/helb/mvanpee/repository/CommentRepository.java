package helb.mvanpee.repository;

import helb.mvanpee.model.Post;
import helb.mvanpee.model.Comment;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.util.List;

@ApplicationScoped
public class CommentRepository {

    @Inject
    EntityManager entityManager;

    public List<Comment> findAll(Long postId) {
        return  (List<Comment>) entityManager.createNamedQuery("Comments.findAll")
                .setParameter("postId", postId)
                .getResultList();
    }

    public Comment findCommentById(Long id) {
        Comment comment = entityManager.find(Comment.class, id);
        if (comment == null) {
            throw new WebApplicationException("Comment with id of " + id + " does not exist.", 404);
        }
        return comment;
    }
    @Transactional
    public void updateComment(Comment comment) {
        Comment commentToUpdate = findCommentById(comment.getId());
        commentToUpdate.setText(comment.getText());
    }
    @Transactional
    public void createComment(Comment comment, Post post) {
        comment.setPost(post);
        entityManager.persist(comment);

    }
    @Transactional
    public void deleteComment(Long commentId) {
        Comment c = findCommentById(commentId);
        entityManager.remove(c);
    }
}
